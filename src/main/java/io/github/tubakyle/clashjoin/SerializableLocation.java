package io.github.tubakyle.clashjoin;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kyle Sferrazza on 8/5/2014.
 * This file is a part of: ClashJoin.
 * All rights reserved.
 */
public class SerializableLocation implements ConfigurationSerializable {
    private String worldName;
    private double x, y, z;

    public SerializableLocation(Location location) {
        this(location.getWorld().getName(), location.getX(), location.getY(), location.getZ());
    }

    public SerializableLocation(String worldName, double x, double y, double z) {
        this.worldName = worldName;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Location getLocation() {
        return new Location(Bukkit.getWorld(worldName), x, y, z);
    }

    public void setWorldName(String worldName) {
        this.worldName = worldName;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public String getWorldName() {
        return worldName;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<String, Object>(4);

        map.put("world", worldName);
        map.put("x", x);
        map.put("y", y);
        map.put("z", z);

        return map;
    }

    public static SerializableLocation deserialize(Map<String, Object> map) {
        return new SerializableLocation((String) map.get("world"), (Double) map.get("x"), (Double) map.get("y"), (Double) map.get("z"));
    }
}
