package io.github.tubakyle.clashjoin;

/**
 * Created by Kyle Sferrazza on 7/30/2014.
 * All rights reserved.
 */
public enum MiniGame {
    CTF("CTF", "ctf join " , "CTF", 12),
    MOBARENA("MobArena", "ma join ", "mobarena", 12),
    OITC("OITC", "oitc join ", "ClashOITC", 12),
    JAILCLASH("JailClash", "server jailclash", "null", 12),
    COLORSHUFFLE("ColorShuffle", "color join ", "colorshuffle", 6);

    private final String gameName;
    private final String command;
    private final String worldName;
    private final int maxPlayers;

    MiniGame(String gameName, String command, String worldName, int maxPlayers) {
        this.gameName = gameName;
        this.command = command;
        this.worldName = worldName;
        this.maxPlayers = maxPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public String getWorldName() {
        return worldName;
    }

    public String getCommand() {
        return command;
    }

    public String getGameName() {
        return gameName;
    }
}