package io.github.tubakyle.clashjoin;

import net.md_5.bungee.api.ProxyServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kyle Sferrazza on 7/29/2014.
 * This file is a part of: ClashJoin.
 * All rights reserved.
 */
@SuppressWarnings("ALL")
public class ClashJoin extends JavaPlugin implements Listener {
    private List<SerializableLocation> signs;

    @Override
    public void onLoad() {
        ConfigurationSerialization.registerClass(SerializableLocation.class);
    }

    @Override
    public void onEnable() {
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        //getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", t)
        saveDefaultConfig();
        signs = new ArrayList<SerializableLocation>();
        int keynum = 1;
        for (String key : getConfig().getKeys(true)) {
            keynum++;
        }

        if(getConfig().isList("signs")) {
            //noinspection unchecked
            List<SerializableLocation> serializedLocations = (List<SerializableLocation>) getConfig().getList("signs");
            for (SerializableLocation serializedLocation : serializedLocations) {
                signs.add(serializedLocation);
            }
        }
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        int keynum = 1;
        for (String key : getConfig().getKeys(true)) {
            keynum++;
        }
        reloadConfig();
        keynum = 1;
        for (String key : getConfig().getKeys(true)) {
            keynum++;
        }
        getConfig().set("signs", signs);
        saveConfig();
    }

    //TODO: if online, show players in world.

    @EventHandler
    public void onSignChange(SignChangeEvent sign) {
        Player player = sign.getPlayer();

        //Checking if they are trying to manually create the sign.
        for (MiniGame miniGame : MiniGame.values()) {
            if (sign.getLine(0).equalsIgnoreCase(miniGame.getGameName())) {
                sign.setLine(0, "");
                sign.setLine(1, "");
                sign.setLine(2, "");
                sign.setLine(3, "");
                sendMessage(player, ChatColor.RED + "You can't create a ClashJoin sign like that!");
                return;
            }
        }

        if (sign.getLine(0).equalsIgnoreCase("[ClashJoin]")) {
            if (!(player.hasPermission("clashjoin.create"))) {
                sign.setLine(0, "");
                sign.setLine(1, "");
                sign.setLine(2, "");
                sign.setLine(3, "");
                noPerms(player);
                return;
            }
            MiniGame miniGame = null;
            for (MiniGame game : MiniGame.values()) {
                if (sign.getLine(1).equalsIgnoreCase(game.getGameName())) {
                    miniGame = game;
                }
            }
            if (miniGame == null) {
                sendMessage(player, ChatColor.RED + "Invalid minigame.");
                sign.setLine(0, "");
                sign.setLine(1, "");
                sign.setLine(2, "");
                sign.setLine(3, "");
                return;
            }
            String arenaName = sign.getLine(2);
            String online = sign.getLine(3);
            SerializableLocation signLoc = new SerializableLocation(sign.getBlock().getLocation());

            signs.add(signLoc);

            sign.setLine(0, miniGame.getGameName());
            sign.setLine(1, ChatColor.BOLD + "===============");
            sign.setLine(2, ChatColor.BLUE + arenaName);
            if (online.equalsIgnoreCase("offline")) {
                sign.setLine(3, ChatColor.DARK_RED + "offline");
                return;
            }
            int maxPlayers = miniGame.getMaxPlayers();
            int currentPlayers = 0;
            if (miniGame.equals(MiniGame.JAILCLASH)) {
                currentPlayers = ProxyServer.getInstance().getServerInfo("jailclash").getPlayers().size();;
            } else {
                try {
                    World miniGameWorld = getServer().getWorld(miniGame.getWorldName());
                    currentPlayers = miniGameWorld.getPlayers().size();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    sign.setLine(3, ChatColor.RED + "INVALID WORLD.");
                }
            }
            sign.setLine(3, ChatColor.GREEN + "" + currentPlayers + "/" + maxPlayers);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getState() instanceof Sign) {
            Sign sign = ((Sign) event.getClickedBlock().getState());
            MiniGame miniGame = null;
            for (MiniGame mode : MiniGame.values()) {
                if (sign.getLine(0).equalsIgnoreCase(mode.getGameName())) {
                    miniGame = mode;
                }
            }
            if (miniGame == null) {
                return;
            }
            if (ChatColor.stripColor(sign.getLine(3)).equalsIgnoreCase("offline")) {
                sendMessage(player, ChatColor.RED + "That minigame is offline!");
                return;
            }
            World miniGameWorld = getServer().getWorld(miniGame.getWorldName());
            int currentPlayers = miniGameWorld.getPlayers().size();
            int maxPlayers = miniGame.getMaxPlayers();
            if (currentPlayers >= maxPlayers) {
                sendMessage(player, ChatColor.RED + "That minigame is full!");
                return;
            }
            String arenaName = ChatColor.stripColor(sign.getLine(2));
            player.chat("/" + miniGame.getCommand() + arenaName);
        }
    }

    public void updateAll() {
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
            @Override
            public void run() {
                for (SerializableLocation serializableLocation : signs) {
                    Sign s = (Sign) serializableLocation.getLocation().getBlock().getState();
                    for (MiniGame game : MiniGame.values()) {
                        if (s.getLine(0).equalsIgnoreCase(game.getGameName())) {
                            if (!(ChatColor.stripColor(s.getLine(3)).equalsIgnoreCase("offline"))) {
                                updatePlayersOnSign(s, game);
                            }
                        }
                    }
                }
            }
        }, 10);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Location breakLoc = event.getBlock().getLocation();
        if (signs.isEmpty() | !(breakLoc.getBlock().getState() instanceof  Sign)) {
            return;
        }
        List<SerializableLocation> newSigns = new ArrayList<SerializableLocation>();
        newSigns.addAll(signs);
        for (SerializableLocation serializableLocation : signs) {
            if (breakLoc.equals(serializableLocation.getLocation())) {
                newSigns.remove(serializableLocation);
            }
        }
        signs.clear();
        signs.addAll(newSigns);
    }

    @EventHandler
    public void onWorldChange(PlayerChangedWorldEvent event) {
        updateAll();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        updateAll();
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        updateAll();
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        updateAll();
    }

    public void updatePlayersOnSign(Sign sign, MiniGame miniGame) {
        try {
            World miniGameWorld = getServer().getWorld(miniGame.getWorldName());
            int currentPlayers = miniGameWorld.getPlayers().size();
            int maxPlayers = miniGame.getMaxPlayers();
            sign.setLine(3, ChatColor.GREEN + "" + currentPlayers + "/" + maxPlayers);
            if (currentPlayers == maxPlayers) {
                sign.setLine(3, ChatColor.RED + "FULL");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            sign.setLine(3, ChatColor.RED + "INVALID WORLD.");
        }
        sign.update();
    }

    public void sendMessage(Player player, String msg) {
        player.sendMessage(ChatColor.GOLD + "[ClashJoin] " + ChatColor.RESET + msg);
    }

    public void noPerms(Player player) {
        sendMessage(player, ChatColor.RED + "You don't have permission to create a ClashJoin sign.");
    }
}
